﻿using UnityEngine;

[RequireComponent(typeof(IDamagable))]
[RequireComponent(typeof(Collider))]
public class CollisionDamage : MonoBehaviour
{
    private IDamagable _damagableObject;

    private void Awake()
    {
        if (TryGetComponent(out _damagableObject) == false)
        {
            Error.MissingComponent("IDamageble object", "CollisionDamage");
        }
    }

    void OnCollisionStay(Collision collisionObject)
    {
        if (collisionObject.gameObject.TryGetComponent(out IDamaging damagingObject))
        {
            float damage = damagingObject.DamagePerSecond * Time.fixedDeltaTime;
            _damagableObject.ReduceHealtPoint(damage);
        }
    }
}