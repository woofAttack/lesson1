using UnityEngine;

// ������� ��������� � ������ �������
public interface IDamaging
{
    float DamagePerSecond { get; }
}

public class DamagingObject : MonoBehaviour, IDamaging
{
    [field:SerializeField] public float DamagePerSecond { get; private set; }
}