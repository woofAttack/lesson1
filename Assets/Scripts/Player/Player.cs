﻿using System;
using UnityEngine;

public interface IDamagable
{
    void ReduceHealtPoint(float value);
}

public class Player : MonoBehaviour, IDamagable
{
    [SerializeField] private float _healthPoint = 100f;
    [SerializeField] private float _limitHealthPoint = 150f;

    public event Action OnDeath;
    public event Action OnHealthLimitReached;

    public event Action<float> OnHealthPointChange;
    public event Action<float> OnApplyChangeHealthPoint;

    private void Awake()
    {
        if (_healthPoint <= 0f)
        {
            throw new Exception($"Player Health Point equals or less zero. Set the value above zero!");
        }

        if (_healthPoint > _limitHealthPoint)
        {
            throw new Exception($"Player Health Point must not exceed the Limit Health Point.");
        }
    }

    private void Start()
    {
        OnHealthPointChange?.Invoke(_healthPoint);
    }

    public void ReduceHealtPoint(float value)
    {
        if (_healthPoint > 0f)
        {
            float correctValue = Mathf.Min(_healthPoint, value);
            AppleChangeHealthPoint(-correctValue);
        }
    }
    public void AddHealthPoint(float value)
    {
        if (_healthPoint < _limitHealthPoint)
        {
            float correctValue = Mathf.Min(_limitHealthPoint - _healthPoint, value);
            AppleChangeHealthPoint(correctValue);
        }
        else
        {
            OnHealthLimitReached?.Invoke();
        }
    }

    private void AppleChangeHealthPoint(float value)
    {
        _healthPoint += value;

        OnHealthPointChange?.Invoke(_healthPoint);
        OnApplyChangeHealthPoint?.Invoke(value);

        CheckStatePlayer();
    }
    private void CheckStatePlayer()
    {
        if (_healthPoint <= 0)
        {
            OnDeath?.Invoke();
        }
    }
}
