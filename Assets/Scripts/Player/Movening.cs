using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movening : MonoBehaviour
{
    [SerializeField] private float WalkSpeed = 4f;
    [SerializeField] private float CrouchWalkSpeed = 2f;
    [SerializeField] private float MidairSteerAccel = 1.5f;
    [SerializeField] private float JumpForce = 100f;

    private interface IMoveMode
    {
        void OnJump();
        void OnCrouch();
        float GetSpeed(float direction);
    }

    [SerializeField] private IMoveMode currentMoveMode;

    private IMoveMode moveWalk;
    private IMoveMode moveCrouch;
    private IMoveMode moveMidair;

    private float distToGround;
    private Vector3 halfLenght;
    [SerializeField] private bool wasInMidair;
    private Rigidbody rigidBody;
    [SerializeField] private float lastVelocityX;
    [SerializeField] private float jumpVelocityX;


    private void Awake()
    {
        moveWalk = new MoveModeWalk { owner = this };
        moveCrouch = new MoveModeCrouch { owner = this };
        moveMidair = new MoveModeMidair { owner = this };
        
        currentMoveMode = moveWalk;
        rigidBody = GetComponent<Rigidbody>();

        var bounds = GetComponent<BoxCollider>().bounds;
        distToGround = bounds.extents.y + 0.05f;
        halfLenght = new Vector3(bounds.extents.x, 0, 0);
    }

    //Should be called in Update() to properly process inputs and calculate expected velocity
    public void Move(float direction, bool jumpRequested, bool crouchRequested)
    {
        if(jumpRequested)
            currentMoveMode.OnJump();
        else if(crouchRequested)
            currentMoveMode.OnCrouch();

        JumpUpdate();



        //fix bug sticking to wall during jump 
        var pos = transform.position;
        var playerScaleY = transform.localScale.y * 0.5f - 0.01f;

        var upperApexPosition = new Vector3(pos.x, pos.y + playerScaleY, pos.z);
        var lowerApexPosition = new Vector3(pos.x, pos.y - playerScaleY, pos.z);

        var canLeftCollisionWall = 
            Physics.Raycast(upperApexPosition, Vector3.left, out _, halfLenght.x + 0.01f) ||
            Physics.Raycast(lowerApexPosition, Vector3.left, out _, halfLenght.x + 0.01f) ||
            Physics.Raycast(pos, Vector3.left, out _, halfLenght.x + 0.01f);
        var canRightCollisionWall = 
            Physics.Raycast(upperApexPosition, Vector3.right, out _, halfLenght.x + 0.01f) ||
            Physics.Raycast(lowerApexPosition, Vector3.right, out _, halfLenght.x + 0.01f) ||
            Physics.Raycast(pos, Vector3.right, out _, halfLenght.x + 0.01f); // bad practice

        float velocityX = currentMoveMode.GetSpeed(direction);

        if ((velocityX > 0f && canRightCollisionWall == false) || 
            (velocityX < 0f && canLeftCollisionWall == false))
        {
            lastVelocityX = velocityX;
        }
        else
        {
            lastVelocityX = 0f;
            jumpVelocityX = 0f;
        }

        if (lastVelocityX > 6f) lastVelocityX = 6f;
        if (lastVelocityX < -6f) lastVelocityX = -6f;

    }

    //Applying calculated velocity in FixedUpdate so it's in sync with the rest of physics engine
    private void FixedUpdate()
    {
        var velocity = rigidBody.velocity;
        velocity.x = lastVelocityX;
        rigidBody.velocity = velocity;
    }

    private void JumpStart(float force)
    {
        rigidBody.AddForce(force*Vector3.up);
        jumpVelocityX = lastVelocityX;
        currentMoveMode = moveMidair;
        wasInMidair = true;
    }

    private void JumpLand()
    {
        currentMoveMode = moveWalk;
        wasInMidair = false;
    }

    private void JumpUpdate()
    {
        if(!wasInMidair && IsInMidair())
            JumpStart(force: 0f); //start a free fall if we reach a cliff
        else if(wasInMidair && !IsInMidair())
            JumpLand(); //land once we reach the ground
    }

    private void CrouchingStart()
    {
        transform.localScale = Vector3.one;
        transform.localPosition += 0.5f * Vector3.down;
        currentMoveMode = moveCrouch;
    }

    private void CrouchingStop()
    {
        //fix "head ceiling" bug
        var pos = transform.position;
        if (Physics.Raycast(pos, Vector3.up, out _, distToGround)) return;

        transform.localScale = new Vector3(1.0f, 2.0f, 1.0f);
        transform.localPosition += 0.5f * Vector3.up;
        currentMoveMode = moveWalk;
    }

    private bool IsInMidair()
    {
        var pos = transform.position;

        var rightPosition = new Vector3(pos.x + halfLenght.x - 0.01f, pos.y, pos.z);
        var leftPosition = new Vector3(pos.x - halfLenght.x + 0.01f, pos.y, pos.z);
        //We are in midair if neither our front nor our back is on the ground
        return
            !Physics.Raycast(rightPosition, Vector3.down, out _, distToGround) &&
            !Physics.Raycast(leftPosition, Vector3.down, out _, distToGround);
    }

    private abstract class MoveModeBase : IMoveMode
    {
        public Movening owner;
        
        public abstract void OnJump();
        public abstract void OnCrouch();
        public abstract float GetSpeed(float direction);
    }

    private class MoveModeWalk : MoveModeBase
    {
        public override void OnJump()
        {
            owner.JumpStart(owner.JumpForce);
        }

        public override void OnCrouch()
        {
            owner.CrouchingStart();
        }

        public override float GetSpeed(float direction)
        {
            return direction*owner.WalkSpeed;
        }
    }

    private class MoveModeCrouch : MoveModeBase
    {
        //Both jump and crouch buttons make you stop crouching 
        public override void OnJump()
        {
            owner.CrouchingStop();
        }

        public override void OnCrouch()
        {
            owner.CrouchingStop();
        }

        public override float GetSpeed(float direction)
        {
            return direction*owner.CrouchWalkSpeed;
        }
    }

    private class MoveModeMidair : MoveModeBase
    {
        //Not allowing neither double jumps nor crouching in midair for now, doing nothing here
        public override void OnJump() { }
        public override void OnCrouch() { }

        public override float GetSpeed(float direction)
        {
            owner.jumpVelocityX += direction * owner.MidairSteerAccel * Time.fixedDeltaTime;
            return owner.jumpVelocityX;
        }
    }
}
