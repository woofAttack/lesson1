﻿using System;
using UnityEngine;

public class PlayerInputSystem : MonoBehaviour
{
    [SerializeField] private Movening _playerMovening;

    public event Action OnPauseButtonPressed;
    public event Action OnRestartButtonPressed;

    private IGameInput _input;
    private bool _isEnable = true;

    void Awake()
    {
        _input = new GameInputSimpleKeyboard();
        AssignPlayer();
    }

    void AssignPlayer()
    {
        if (_playerMovening == null)
            _playerMovening = FindObjectOfType<Movening>();
        if (_playerMovening == null)
            Error.MissingComponent("movening component");
    }

    void Update()
    {
        if (_input.IsPausePressed()) OnPauseButtonPressed?.Invoke();
        if (_input.IsRestartPressed()) OnRestartButtonPressed?.Invoke();

        if (_isEnable == false)
            return;

        _playerMovening.Move(_input.GetMovementDirection(), _input.IsJumpPressed(), _input.IsCrouchPressed());
    }

    public void SetEnableState(bool state)
    {
        _isEnable = state;
    }
}