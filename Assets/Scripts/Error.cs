﻿using System;
using UnityEngine.SceneManagement;

public static class Error
{
    public static void MissingComponent(string label)
    {
        throw new Exception($"Couldn't find a {label} object on this level. Are you sure it exists in the scene {SceneManager.GetActiveScene().name}");
    }
    public static void MissingComponent(string missingObjectName, string ex)
    {
        throw new Exception($"{ex} can't find a {missingObjectName} object. Are you sure it exists in Game Object of {ex}");
    }

    public static void UncorrectTime()
    {
        throw new Exception("Timer sets uncorrect.");
    }
}