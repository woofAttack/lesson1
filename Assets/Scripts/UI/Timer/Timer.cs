using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public event Action OnTimeEnd;

    private bool _isEnable = false;

    private float _secondsLeft;
    private float _percentTimeRemaining;
    private float _deltaTimePercent;
    private List<ITimerView> _timerViews = new List<ITimerView>();

    private void Awake()
    {
        GetComponentsInChildren(_timerViews);
    }
    private void Update()
    {
        if (_isEnable)
        {
            _secondsLeft -= Time.deltaTime;

            if (_secondsLeft <= 0f)
            {
                _secondsLeft = 0f;
                _isEnable = false;
                OnTimeEnd?.Invoke();
            }

            _percentTimeRemaining = _secondsLeft * _deltaTimePercent;

            var _secondLeftCeiled = Mathf.CeilToInt(_secondsLeft).ToString();

            foreach(var timerView in _timerViews)
            {
                timerView.SetSecondsLeftText(_secondLeftCeiled);
                timerView.SetPercentageTimeRemaining(_percentTimeRemaining);
            }
        }
    }

    public void StartTimer(float second)
    {
        if (second <= 0f)
        {
            CloseTimer();
            return;
        }

        gameObject.SetActive(true);

        _isEnable = _timerViews.Count != 0;

        _secondsLeft = second;
        _percentTimeRemaining = 1f;
        _deltaTimePercent = 1f / second;
    }
    public void StopTimer()
    {
        _isEnable = false;
    }
    public void CloseTimer()
    {
        _isEnable = false;
        gameObject.SetActive(false);
    }
}