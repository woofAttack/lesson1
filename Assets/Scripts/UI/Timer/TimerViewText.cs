﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TimerViewText : MonoBehaviour, ITimerView
{
    [SerializeField] private string _format = "{0}";

    private Text _objectTextComponent;

    private void Awake()
    {
        _objectTextComponent = GetComponent<Text>();  
    }

    public void SetPercentageTimeRemaining(float percent) { }

    public void SetSecondsLeftText(string seconds)
    {
        _objectTextComponent.text = string.Format(_format, seconds);
    }
}

