﻿public interface ITimerView
{
    void SetSecondsLeftText(string seconds);
    void SetPercentageTimeRemaining(float percent); // range 0f..1f
}

