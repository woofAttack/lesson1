﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class TimerViewImage : MonoBehaviour, ITimerView
{ 
    private Image _objectImageComponent;

    private void Awake()
    {
        _objectImageComponent = GetComponent<Image>();
    }

    public void SetPercentageTimeRemaining(float percent) 
    {
        _objectImageComponent.fillAmount = percent;
    }

    public void SetSecondsLeftText(string seconds) { }
}

