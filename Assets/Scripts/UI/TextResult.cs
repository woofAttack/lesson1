﻿using UnityEngine;

public class TextResult : MonoBehaviour
{
    [SerializeField] private GameState _currentGameState;

    [SerializeField] private GameObject _buttonRetry;
    [SerializeField] private GameObject _textWon;
    [SerializeField] private GameObject _textLostByDamage;
    [SerializeField] private GameObject _textLostAtTime;
    [SerializeField] private GameObject _textPaused;

    private void Awake()
    {
        if (_currentGameState == null) Error.MissingComponent("game state");
    }

    private void OnEnable()
    {
        _currentGameState.OnChangePauseState += SetPauseText;
        _currentGameState.OnChangeStateGame += SetStateGameText;
        _currentGameState.OnLostAtTimer += SetStateGameLostAtTimer;
    }
    private void OnDisable()
    {
        _currentGameState.OnChangePauseState -= SetPauseText;
        _currentGameState.OnChangeStateGame -= SetStateGameText;
        _currentGameState.OnLostAtTimer -= SetStateGameLostAtTimer;
    }

    private void SetPauseText(bool isPauseState) => _textPaused.SetActive(isPauseState);
    private void SetStateGameText(bool IsWinStateGame)
    {
        _textWon.SetActive(IsWinStateGame);
        _textLostByDamage.SetActive(!IsWinStateGame);

        _buttonRetry.SetActive(true);
    }
    private void SetStateGameLostAtTimer()
    {
        _textLostAtTime.SetActive(true);
        _buttonRetry.SetActive(true);
    }
}