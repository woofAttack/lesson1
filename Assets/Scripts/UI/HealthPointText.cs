﻿using System;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class HealthPointText : MonoBehaviour
{ 
    [SerializeField] private Player _player;
    [SerializeField] private string _textFormat;
    
    private Text _text;

    private void Awake()
    {
        if (TryGetComponent(out _text) == false)
        {
            Error.MissingComponent("text for health point");
        }

        if (_player == null)
            Error.MissingComponent("Player", "HealthPoint Text Component");
    }

    private void OnEnable()
    {
        _player.OnHealthPointChange += SetText;
    }
    private void OnDisable()
    {
        _player.OnHealthPointChange -= SetText;
    }

    //private void SetText(float value) => SetText(((int)value).ToString());
    private void SetText(float value) => SetText(Mathf.CeilToInt(value).ToString());
    private void SetText(string text) => _text.text = String.Format(_textFormat, text);
}
