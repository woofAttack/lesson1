﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameState : MonoBehaviour
{
    [SerializeField] private PlayerInputSystem _playerInputSystem;

    private bool _isPaused = false;
    private bool _gameOver = false;

    public event Action OnLostAtTimer;
    public event Action<bool> OnChangePauseState;
    public event Action<bool> OnChangeStateGame;

    private void Awake()
    {
        if (_playerInputSystem == null)
            _playerInputSystem = FindObjectOfType<PlayerInputSystem>();
        if (_playerInputSystem == null)
            Error.MissingComponent("player input system");
    }

    private void OnEnable()
    {
        _playerInputSystem.OnPauseButtonPressed += TogglePause;
        _playerInputSystem.OnRestartButtonPressed += TryRestart;
    }
    private void OnDisable()
    {
        _playerInputSystem.OnPauseButtonPressed -= TogglePause;
        _playerInputSystem.OnRestartButtonPressed -= TryRestart;
    }

    public bool IsGameStopped() => _isPaused || _gameOver;

    public void TogglePause()
    {
        SetPause(!_isPaused);
    }
    private void SetPause(bool pause)
    {
        PauseGame(pause);
        OnChangePauseState?.Invoke(pause);
    }
    private void PauseGame(bool pause)
    {
        _playerInputSystem.SetEnableState(!pause);
        Time.timeScale = pause ? 0.0f : 1.0f;
        _isPaused = pause;
    }

    public void SetWin() => SetEndGame(true);
    public void SetLost() => SetEndGame(false);
    public void SetLostAtTime()
    {
        _gameOver = true;
        PauseGame(true);

        OnLostAtTimer?.Invoke();
    }
    private void SetEndGame(bool isWinState)
    {
        _gameOver = true;
        PauseGame(true);

        OnChangeStateGame?.Invoke(isWinState);
    }
    private void TryRestart()
    {
        if (_gameOver == false) RestartLevel();
    }
    public void RestartLevel()
    {
        PauseGame(false);
        var level = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(level);
    }

}
