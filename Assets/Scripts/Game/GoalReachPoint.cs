﻿using UnityEngine;

public interface IGoalReach
{
    bool IsComplete();
}

public class GoalReachPoint : MonoBehaviour, IGoalReach
{
    [SerializeField] private Transform _playerTransform;
    [SerializeField] private Transform _goalPosition;

    public bool IsComplete() => _playerTransform.position.x > _goalPosition.transform.position.x;
}