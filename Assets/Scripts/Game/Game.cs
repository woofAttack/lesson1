using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(IGoalReach))]
[RequireComponent(typeof(GameState))]
public class Game : MonoBehaviour
{ 
    [SerializeField] private Player _player;

    [Header("Timer Setup")]
    [SerializeField] private Timer _timer;
    [Tooltip("���������� �������� ������ 0, ���� ������ �� ��������� ������.")]
    [SerializeField] private float _secondsForGame;

    private IGoalReach _levelGoalReach;
    private GameState _gameState;

    private void Awake()
    {
        AssignPlayer();
        AssignTimer();

        if (TryGetComponent(out _levelGoalReach) == false)
            Error.MissingComponent("Goal Reach", "Game Manager");

        if (TryGetComponent(out _gameState) == false)
            Error.MissingComponent("Game State", "Game Manager");
    }

    private void AssignPlayer()
    {
        if (_player == null)
            _player = FindObjectOfType<Player>();
        if (_player == null)
            Error.MissingComponent("player");
    }
    private void AssignTimer()
    {
        if (_timer == null)
            _timer = FindObjectOfType<Timer>();
        if (_timer == null)
            Error.MissingComponent("timer");
    }

    private void Start()
    {
        _timer.StartTimer(_secondsForGame); // Awake �� ���������� ����� ����������� �����.
    }

    private void OnEnable()
    {
        _player.OnDeath += Lose;
        _timer.OnTimeEnd += TimeWasLeft;     
    }
    private void OnDisable()
    {
        _player.OnDeath -= Lose;
        _timer.OnTimeEnd -= TimeWasLeft;
    }

    private void Update()
    {
        if(_gameState.IsGameStopped() == true)
            return;
      
        if(_levelGoalReach.IsComplete() == true)
            Win();
    }

    private void Win()
    {
        _gameState.SetWin();
        _timer.StopTimer();
    }
    private void Lose()
    {
        _gameState.SetLost();
    }

    private void TimeWasLeft()
    {
        _gameState.SetLostAtTime();
    }
}
