﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
public class CameraTracking : MonoBehaviour
{
    [SerializeField] private Transform _objectForTracking;
    [SerializeField] private Transform _objectTrackingMinPosition;
    [SerializeField] private Transform _objectTrackingMaxPosition;

    private Camera _camera;
    private float _objectTrackingMinX;
    private float _objectTrackingMaxX;
    private float _cameraDeltaX;
    private float _cameraDeltaY;

    private void Awake()
    {
        AssignCamera();
    }
    private void AssignCamera()
    {
        if (TryGetComponent(out _camera) == false)
        {
            Error.MissingComponent("camera", "Camera tracking component");
        }

        _objectTrackingMinX = _objectTrackingMinPosition.position.x - 3.0f;
        _objectTrackingMaxX = _objectTrackingMaxPosition.position.x - 3.0f;

        _cameraDeltaX = _camera.transform.position.x - 3.0f;
        _cameraDeltaY = _camera.transform.position.y + 1.0f;
    }
    private void LateUpdate()
    {
        CameraUpdateTrailing();
    }
    
    //Making camera trail the player in LateUpdate because player's new position is ready by then
    private void CameraUpdateTrailing()
    {
        float objectFollowingPositionX = _objectForTracking.position.x;
        float objectFollowingPositionY = _objectForTracking.position.y;

        if (objectFollowingPositionX < _objectTrackingMinX || objectFollowingPositionX > _objectTrackingMaxX)
            return;

        var cameraTransform = _camera.transform;
        var cameraPosition = cameraTransform.position;

        cameraPosition.x = objectFollowingPositionX + _cameraDeltaX;
        cameraPosition.y = objectFollowingPositionY + _cameraDeltaY;

        cameraTransform.position = cameraPosition;
    }
}